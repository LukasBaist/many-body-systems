#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <mpi.h>

using namespace std;

#pragma region global settings

// Gravitational constant in m^3/(kg*s^2)
//double G = 0.0000000000667430
double G = 1.0;

#pragma endregion

#pragma region classes & structs

struct vec3d
{
	double x;
	double y;
	double z;
};

#pragma endregion

#pragma region function declarations

#pragma region output

void clearFile(string filename);

// Prints coords and momenta of mass points
void printSystem(string filename, long N, double* coords, double* momenta);

// Computes and prints the energy of the system (kinetic, potential, total)
void printEnergies(string filename, long N, double* masses, double* coords, double* momenta);

// Prints masses of mass points
void printMasses(string filename, long N, double* masses);

// Prints main config settings to output file
void printConfig(string filename, long N, int size, long N_loc, bool hyper, int steps, int printStep, double delta, double epsilon);

#pragma endregion

#pragma region init & clear

#pragma region read Config

#pragma region parts

// Function to split a string by a certain delimiter
vector<string> split(const string& s, char delimiter);

void readNumMasspoints(ifstream& stream, long &N);

void readMasses(ifstream& stream, vector<double> &masses);

void readMassRange(ifstream& stream, vector<double> &range);

vector<double> readRange(ifstream& stream);

void readArray(ifstream& stream, vector<vec3d> &arr);

void initArrays_Root(long N, long N_loc, double*& masses, double*& coords, double*& coords_loc, double*& coords_step, double*& momenta, double*& momenta_loc, double*& force);

void initArrays_Worker(long N, long N_loc, double*& masses, double*& coords_loc, double*& coords_step, double*& momenta_loc, double*& force);

void initArrays_Root_Hyper(long N, long N_loc, int layers, double* &masses, double* &coords, double* &coords_loc, double* &coords_step, double* &momenta, double* &momenta_loc, double* &force);

void initArrays_Worker_Hyper(long N, long N_loc, int layers, double* &masses, double* &coords_loc, double* &coords_step, double* &momenta_loc, double* &force);

#pragma endregion

void readConfigClusters(string filename, long N, double*& masses, double*& coords, double*& momenta);

void readConfigRand(string filename, long N, double*& masses, double*& coords, double*& momenta);

void readConfigFull(string filename, long N, double*& masses, double*& coords, double*& momenta);

#pragma endregion

void init(string configFilename, bool hyper, int size, int &printStep, int &steps, double &delta, double &epsilon, long &N, long &N_loc, int &layers, double* &masses, double* &coords, double* &coords_loc, double* &coords_step, double* &momenta, double* &momenta_loc, double* &force);

void clear_Arrays_Worker(double* &masses, double* &coords_loc, double* &coords_step, double* &momenta_loc, double* &force);

#pragma endregion

#pragma region physics

// Gravitive force, eps is used to smoothen the potential and increase system stability
void force_grav(double* a, double* b, double m_a, double m_b, double eps, double* res);

void force_Combination(int rank_1, int rank_2, int force_index, double* coords_step_1, double* coords_step_2, double t, double eps, long N_loc, double* masses, double* force, double* force_temp);

void force_Combination_DualIndex(int rank, int rank_1, int rank_2, int force_index_1, int force_index_2, double* coords_step_1, double* coords_step_2, double t, double eps, long N_loc, double* masses, double* force, double* force_temp);

void force_Combination_Single(int rank_1, int rank_2, double* coords_step_1, double* coords_step_2, double t, double eps, long N_loc, double* masses, double* force, double* force_temp);

#pragma endregion

#pragma region integrator

// Serial Leapfrog
void leap(double t, double eps, long N, double* masses, double* coords, double* coords_step, double* momenta, double* force, double* force_temp);

// Parallel Leapfrog - Systolic
void leap_MPI(int rank, int size, MPI_Status& status, int* neighbour_right, int* neighbour_left, double t, double eps, long N_loc, double* masses, double* coords_loc, double* coords_step, double* momenta_loc, double* force, double* force_temp);

// Parallel Leapfrog - Hypersystolic (8 Processes)
void leap_MPI_Hyper_8(int rank, MPI_Status &status, int layers, int* shift_vector, int* neighbour_right, int* neighbour_left, double t, double eps, long N_loc, double* masses, double* coords_loc, double* coords_step, double* momenta_loc, double* force, double* force_temp);

// Parallel Leapfrog - Hypersystolic (16 Processes)
void leap_MPI_Hyper_16(int rank, MPI_Status& status, int layers, int* shift_vector, int* neighbour_right, int* neighbour_left, double t, double eps, long N_loc, double* masses, double* coords_loc, double* coords_step, double* momenta_loc, double* force, double* force_temp);

#pragma endregion

#pragma endregion

#pragma region main

int main(int argc, char* argv[])
{	
	#pragma region settings

	// Choose between 'manual' compiler (e.g. used on cluster) or VisualStudio compiler with local file system
	bool manual_compiler = false;
	string outfolder = "../../../Analysis/Outputs/";
	string timingfolder = "../../../Analysis/Timing/";
	if (manual_compiler)
	{
		outfolder = "Analysis/Outputs/";
		timingfolder = "Analysis/Timing/";
	}	

	// Config
	string configFile = "../../../Configuration/Configs/";
	if (manual_compiler)
	{
		configFile = "Configs/";
	}
	
	// Integration (standard values)
	int steps = 10000;
	int printStep = 1;
	double delta = 0.0001;
	double epsilon = pow(10, -10);

	#pragma endregion

	MPI_Init(&argc, &argv);

	#pragma region variables

	// MPI variables
	int rank, size;
	MPI_Status status;

	// Hypersystolic - Number of layers needed for communication (size of shift-vector + 1)
	int layers = 1;
	int* shift_vector = nullptr;
	bool hyper = false;				// Bool used to switch between systolic and hypersystolic variant
	// Number of mass points
	long N = 0;						// Total number
	long N_loc = 0;					// Number handled by each process
	// Arrays for mass points
	double* masses = nullptr;
	double* coords_loc = nullptr;
	double* coords_step = nullptr;	// Coordinates at half step in Leap-Frog
	double* momenta_loc = nullptr;
	double* force = nullptr;
	// Internal usage
	double force_temp[3];

	// Only used for root
	double* coords = nullptr;		// Containing all 3*N coordinates for initialization and output
	double* momenta = nullptr;		// Containing all 3*N momenta for initialization and output

	string outfile;
	string outfile_energy;
	string outfile_timing;	

	#pragma endregion

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	

	MPI_Barrier(MPI_COMM_WORLD);
	double t_start = MPI_Wtime();

	#pragma region initialisation

	// The initialization is done by the root process
	if (rank == 0)
	{
		// Determine the shift-vector depending on the number of processes
		switch (size)
		{		
		case 8:
			layers = 4;
			shift_vector = new int[3];
			shift_vector[0] = 1;
			shift_vector[1] = 1;
			shift_vector[2] = 2;
			hyper = true;
			break;

		case 16:
			layers = 5;
			shift_vector = new int[4];
			shift_vector[0] = 1;
			shift_vector[1] = 1;
			shift_vector[2] = 3;
			shift_vector[3] = 3;
			hyper = true;
			break;

		default:
			hyper = false;
			break;
		}

		// Read the config name from the command line argument
		string name;
		string output_addition = "";
		if (argc > 1)
		{
			name = argv[1];
			if (argc > 2)
			{
				output_addition = argv[2];
			}
		}
		else
		{
			cout << "No config-file given!" << endl;
		}
		configFile = configFile + name + ".txt";
		
		try
		{
			// Call the initialization routine that reads the necessary data from the config file
			init(configFile, hyper, size, printStep, steps, delta, epsilon, N, N_loc, layers, masses, coords, coords_loc, coords_step, momenta, momenta_loc, force);
			
			// Create the appendix for the output files containing the setup data
			//char appendix[200];			
			// VisualStudio
			//sprintf_s(appendix, "_N-%d_steps-%d_printStep-%d_delta-%10.8f_epsilon-%16.14f", N, steps, printStep, delta, epsilon);
			// Manual compilation
			//sprintf(appendix, "_N-%d_steps-%d_printStep-%d_delta-%10.8f_epsilon-%16.14f", N, steps, printStep, delta, epsilon);

			outfile = outfolder + name + "_" + output_addition + "_output.txt";
			outfile_energy = outfolder + name + "_" + output_addition + "_energy.txt";
			outfile_timing = timingfolder + name + "_" + output_addition + "_timing.txt";

			clearFile(outfile);
			clearFile(outfile_energy);

			printConfig(outfile, N, size, N_loc, hyper, steps, printStep, delta, epsilon);
			printMasses(outfile, N, masses);
			printSystem(outfile, N, coords, momenta);
			printEnergies(outfile_energy, N, masses, coords, momenta);
		}
		catch (string e)
		{		
			cout << e << endl;
		}
		catch (exception e)
		{		
			cout << e.what() << endl;
		}
	}

	#pragma endregion

	double t_init;
	
	#pragma region simulation

	// Parallel version
	if (size > 1)
	{
		// Distribute the setup data
		MPI_Bcast(&steps, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(&printStep, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(&delta, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Bcast(&epsilon, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

		MPI_Bcast(&N, 1, MPI_LONG, 0, MPI_COMM_WORLD);
		MPI_Bcast(&N_loc, 1, MPI_LONG, 0, MPI_COMM_WORLD);
		MPI_Bcast(&layers, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(&hyper, 1, MPI_C_BOOL, 0, MPI_COMM_WORLD);

		// Hypersystolic
		if (hyper)
		{
			if (rank != 0)
			{
				initArrays_Worker_Hyper(N, N_loc, layers, masses, coords_loc, coords_step, momenta_loc, force);
				shift_vector = new int[layers - 1];
			}
			MPI_Bcast(shift_vector, layers - 1, MPI_INT, 0, MPI_COMM_WORLD);
		}
		// Systolic
		else if (rank != 0)
		{
			initArrays_Worker(N, N_loc, masses, coords_loc, coords_step, momenta_loc, force);
		}

		MPI_Bcast(masses, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Scatter(coords, 3 * N_loc, MPI_DOUBLE, coords_loc, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Scatter(momenta, 3 * N_loc, MPI_DOUBLE, momenta_loc, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);

		// Compute ranks of right and left neighbours for all communication steps
		int* neighbour_right;
		int* neighbour_left;
		// Hypersystolic
		if (hyper)
		{
			neighbour_right = new int[layers - 1];
			neighbour_left = new int[layers - 1];
			neighbour_right[0] = (rank + shift_vector[0]) % size;
			neighbour_left[0] = (rank - shift_vector[0] + size) % size;
			for (int k = 1; k < layers - 1; k++)
			{
				neighbour_right[k] = (neighbour_right[k - 1] + shift_vector[k]) % size;
				neighbour_left[k] = (neighbour_left[k - 1] - shift_vector[k] + size) % size;
			}
		}
		// Systolic
		else
		{
			neighbour_right = new int[size - 1];
			neighbour_left = new int[size - 1];
			for (int k = 0; k < size - 1; k++)
			{
				neighbour_right[k] = (rank + k+1) % size;
				neighbour_left[k] = (rank - (k+1) + size) % size;
			}
		}

		// Variables used to print the progress of the simulation
		int part = steps / 100;
		int percent = 0;

		MPI_Barrier(MPI_COMM_WORLD);
		t_init = MPI_Wtime();

		// Simulation
		// Root node handles the output, therefore use another for-loop to avoid further branching
		// (Avoid checking for rank==0 in each iteration for all processes)
		if (rank == 0)
		{
			// Hypersystolic
			// (Avoid checking systolic or hypersystolic variant in each iteration)
			if (hyper)
			{
				// 8 Processes
				if (size == 8)
				{
					for (int i = 1; i <= steps; i++)
					{
						leap_MPI_Hyper_8(rank, status, layers, shift_vector, neighbour_right, neighbour_left, delta, epsilon, N_loc, masses, coords_loc, coords_step, momenta_loc, force, force_temp);

						if (i % printStep == 0)
						{
							MPI_Gather(coords_loc, 3 * N_loc, MPI_DOUBLE, coords, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
							MPI_Gather(momenta_loc, 3 * N_loc, MPI_DOUBLE, momenta, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);

							printSystem(outfile, N, coords, momenta);
							printEnergies(outfile_energy, N, masses, coords, momenta);
						}
						// Output progress
						if (i % part == 0)
						{
							percent++;
							cout << "Hyper8 - " << percent << " / 100" << endl;
						}
					}
				}
				// 16 Processes
				else if (size == 16)
				{
					for (int i = 1; i <= steps; i++)
					{
						leap_MPI_Hyper_16(rank, status, layers, shift_vector, neighbour_right, neighbour_left, delta, epsilon, N_loc, masses, coords_loc, coords_step, momenta_loc, force, force_temp);

						if (i % printStep == 0)
						{
							MPI_Gather(coords_loc, 3 * N_loc, MPI_DOUBLE, coords, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
							MPI_Gather(momenta_loc, 3 * N_loc, MPI_DOUBLE, momenta, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);

							printSystem(outfile, N, coords, momenta);
							printEnergies(outfile_energy, N, masses, coords, momenta);
						}
						// Output progress
						if (i % part == 0)
						{
							percent++;
							cout << "Hyper16 - " << percent << " / 100" << endl;
						}
					}
				}
				// Other number of processes (should not appear together with hyper=true)
				else
				{
					cout << "Invalid input, currently only 8 or 16 processes are supported in hypersystolic mode!" << endl;
					cout << "All other numbers of processes should be computed in systolic mode!" << endl << endl;
				}				
			}
			// Systolic
			else
			{
				for (int i = 1; i <= steps; i++)
				{
					leap_MPI(rank, size, status, neighbour_right, neighbour_left, delta, epsilon, N_loc, masses, coords_loc, coords_step, momenta_loc, force, force_temp);

					if (i % printStep == 0)
					{
						MPI_Gather(coords_loc, 3 * N_loc, MPI_DOUBLE, coords, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
						MPI_Gather(momenta_loc, 3 * N_loc, MPI_DOUBLE, momenta, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);

						printSystem(outfile, N, coords, momenta);
						printEnergies(outfile_energy, N, masses, coords, momenta);
					}
					// Output progress
					if (i % part == 0)
					{
						percent++;
						cout << "Systolic - " << percent << " / 100" << endl;
					}
				}
			}
		}
		// Worker for-loop
		else
		{
			// Hypersystolic
			// (Avoid checking systolic or hypersystolic variant in each iteration)
			if (hyper)
			{
				// 8 Processes
				if (size == 8)
				{
					for (int i = 1; i <= steps; i++)
					{
						leap_MPI_Hyper_8(rank, status, layers, shift_vector, neighbour_right, neighbour_left, delta, epsilon, N_loc, masses, coords_loc, coords_step, momenta_loc, force, force_temp);

						if (i % printStep == 0)
						{
							MPI_Gather(coords_loc, 3 * N_loc, MPI_DOUBLE, coords, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
							MPI_Gather(momenta_loc, 3 * N_loc, MPI_DOUBLE, momenta, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
						}
					}
				}
				// 16 Processes
				else if (size == 16)
				{
					for (int i = 1; i <= steps; i++)
					{
						leap_MPI_Hyper_16(rank, status, layers, shift_vector, neighbour_right, neighbour_left, delta, epsilon, N_loc, masses, coords_loc, coords_step, momenta_loc, force, force_temp);

						if (i % printStep == 0)
						{
							MPI_Gather(coords_loc, 3 * N_loc, MPI_DOUBLE, coords, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
							MPI_Gather(momenta_loc, 3 * N_loc, MPI_DOUBLE, momenta, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
						}
					}
				}
			}
			// Systolic
			else
			{
				for (int i = 1; i <= steps; i++)
				{
					leap_MPI(rank, size, status, neighbour_right, neighbour_left, delta, epsilon, N_loc, masses, coords_loc, coords_step, momenta_loc, force, force_temp);

					if (i % printStep == 0)
					{
						MPI_Gather(coords_loc, 3 * N_loc, MPI_DOUBLE, coords, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
						MPI_Gather(momenta_loc, 3 * N_loc, MPI_DOUBLE, momenta, 3 * N_loc, MPI_DOUBLE, 0, MPI_COMM_WORLD);
					}
				}
			}
		}		
	}
	// Serial version
	else
	{
		// Variables used to print the progress of the simulation
		int part = steps / 100;
		int percent = 0;

		MPI_Barrier(MPI_COMM_WORLD);
		t_init = MPI_Wtime();

		for (int i = 1; i <= steps; i++)
		{
			leap(delta, epsilon, N, masses, coords, coords_step, momenta, force, force_temp);

			// Print to file only every printStep iteration
			if (i % printStep == 0)
			{
				printSystem(outfile, N, coords, momenta);
				printEnergies(outfile_energy, N, masses, coords, momenta);
			}

			// Output progress
			if (i % part == 0)
			{
				percent++;
				cout << "Serial - " << percent << " / 100" << endl;
			}
		}
	}	
	
	#pragma endregion

	#pragma region finalization
	
	// Free memory
	if (rank == 0)
	{		
		delete coords;
		delete momenta;
	}
	clear_Arrays_Worker(masses, coords_loc, coords_step, momenta_loc, force);

	MPI_Barrier(MPI_COMM_WORLD);
	double t_finish = MPI_Wtime();

	// Display the timing data and write it to file
	if (rank == 0)
	{
		cout << endl << endl;
		cout << "-----------------------------------------" << endl;
		cout << "Total time: " << t_finish - t_start << endl;
		cout << "Init time: " << t_init - t_start << endl;
		cout << "Calc time: " << t_finish - t_init << endl;
		cout << "Process Calc time total: " << (t_finish - t_init) * (double) size << endl;
		cout << "-----------------------------------------" << endl;

		// Write timing data to file
		if (!outfile_timing.empty())
		{
			ofstream outfile;
			outfile.open(outfile_timing, std::ios_base::app);

			if (outfile.is_open())
			{
				outfile << "Total time: " << t_finish - t_start << endl;
				outfile << "Init time: " << t_init - t_start << endl;
				outfile << "Calc time: " << t_finish - t_init << endl;
				outfile << "Process Calc time total: " << (t_finish - t_init) * (double)size << endl;
				outfile.close();
			}
			else
			{
				cout << "Cannot open file given filename for writing timing data: " << outfile_timing << endl;
			}
		}
		else
		{
			cout << "No timing filename given!" << endl;
		}
	}

	#pragma endregion

	MPI_Finalize();	
}

#pragma endregion

#pragma region functions & methods

#pragma region output

// Clears an existing file or create the file 
void clearFile(string filename)
{
	ofstream outfile;
	outfile.open(filename);
	outfile.close();
}

// Prints coords and momenta of mass points
void printSystem(string filename, long N, double* coords, double* momenta)
{
	if (coords != nullptr)
	{
		if (momenta != nullptr)
		{
			// If filename is given, try to write to file
			if (!filename.empty())
			{
				ofstream outfile;
				outfile.open(filename, std::ios_base::app);

				if (outfile.is_open())
				{
					for (int i = 0; i < N; i++)
					{
						outfile << scientific << setprecision(16)
							<< setw(17) << coords[i * 3 + 0] << setw(1) << " "
							<< setw(17) << coords[i * 3 + 1] << setw(1) << " "
							<< setw(17) << coords[i * 3 + 2] << setw(1) << " "
							<< setw(17) << momenta[i * 3 + 0] << setw(1) << " "
							<< setw(17) << momenta[i * 3 + 1] << setw(1) << " "
							<< setw(17) << momenta[i * 3 + 2] << endl;
					}
					outfile << "---" << endl;
					outfile.close();
				}
				else
				{
					throw string{ "Cannot open file given filename for writing data: \t" + filename };
				}
			}
			else
			{
				throw string{ "No output filename given!" };
			}
		}
		else
		{
			throw string{ "Velocity-Array was not set correctly (pointer was nullptr)" };
		}
	}
	else
	{
		throw string{ "Coordinate-Array was not set correctly (pointer was nullptr)" };
	}	
}

// Computes and prints the energy of the system (kinetic, potential, total)
void printEnergies(string filename, long N, double* masses, double* coords, double* momenta)
{
	double E_kin = 0;
	double E_pot = 0;
	double E_tot;

	for (int i = 0; i < N; i++)
	{
		E_kin = E_kin + (pow(momenta[i * 3 + 0], 2) + pow(momenta[i * 3 + 1], 2) + pow(momenta[i * 3 + 2], 2)) / masses[i];

		for (int j = 0; j < i; j++)
		{
			E_pot = E_pot + masses[i] * masses[j] /
				sqrt(pow(coords[i * 3 + 0] - coords[j * 3 + 0], 2)
					+ pow(coords[i * 3 + 1] - coords[j * 3 + 1], 2)
					+ pow(coords[i * 3 + 2] - coords[j * 3 + 2], 2));
		}
	}

	E_kin = E_kin * 0.5;
	E_pot = -E_pot * G;
	E_tot = E_kin + E_pot;

	// If filename is given, try to write to file
	if (!filename.empty())
	{
		ofstream outfile;
		outfile.open(filename, std::ios_base::app);

		if (outfile.is_open())
		{
			outfile << scientific << setprecision(16)
				<< setw(10) << E_kin << setw(3) << " | "
				<< setw(10) << E_pot << setw(3) << " | "
				<< setw(10) << E_tot << endl;
			outfile.close();
		}
		else
		{
			throw string{ "Cannot open file given filename for writing data: \t" + filename };
		}
	}
	else
	{
		throw string{ "No energy filename given!" };
	}
}

void printMasses(string filename, long N, double* masses)
{
	if (masses != nullptr)
	{
		// If filename is given, try to write to file
		if (!filename.empty())
		{
			ofstream outfile;
			outfile.open(filename, std::ios_base::app);

			if (outfile.is_open())
			{
				for (int i = 0; i < N; i++)
				{
					outfile << scientific << setprecision(6) << masses[i] << endl;
				}
				outfile << "---" << endl;
				outfile.close();
			}
			else
			{
				throw string{ "Cannot open file given filename for writing data: \t" + filename };
			}
		}
		else
		{
			throw string{ "No output filename given!" };
		}
	}
	else
	{
		throw string{ "Mass-Array was not set correctly (pointer was nullptr)" };
	}
}

void printConfig(string filename, long N, int size, long N_loc, bool hyper, int steps, int printStep, double delta, double epsilon)
{
	// If filename is given, try to write to file
	if (!filename.empty())
	{
		ofstream outfile;
		outfile.open(filename, std::ios_base::app);

		if (outfile.is_open())
		{
			outfile << fixed;
			outfile << "N\t\t" << N << endl;
			outfile << "Size\t\t" << size << endl;
			outfile << "N_loc\t\t" << N_loc << endl;
			outfile << "Hyper\t\t" << hyper << endl;
			outfile << "Steps\t\t" << steps << endl;
			outfile << "PrintStep\t" << printStep << endl;
			outfile << scientific;
			outfile << "Delta\t\t" << delta << endl;
			outfile << "Epsilon\t\t" << epsilon << endl;
			outfile << "---" << endl;
			outfile.close();
		}
		else
		{
			throw string{ "Cannot open file given filename for writing data: \t" + filename };
		}
	}
	else
	{
		throw string{ "No output filename given!" };
	}
}

#pragma endregion

#pragma region init & clear

#pragma region read Config

#pragma region parts

// Splits a string by a certain delimiter, returns the parts
vector<string> split(const string& s, char delimiter) {
	vector<string> parts;
	stringstream stream(s);
	string element;
	while (getline(stream, element, delimiter))
	{
		parts.push_back(element);
	}
	return parts;
}

void readNumMasspoints(ifstream& stream, long &N)
{
	string line;

	try
	{
		getline(stream, line);

		long temp = stol(line);
		if (temp > 0)
		{
			N = temp;
		}
		else
		{
			throw string{ "N was negative" };
		}
	}
	catch (exception e)
	{
		throw e;
	}
}

void readMasses(ifstream& stream, vector<double> &masses)
{
	string line;

	try
	{
		while (getline(stream, line))
		{
			// If line is empty, all masses have been read in -> break loop
			// Otherwise try to transform line into double and add it to masses-vector
			if (!line.empty())
			{
				try
				{
					double temp = stof(line);

					masses.push_back(temp);
				}
				catch (exception e)
				{
					throw e;
				}
			}
			else
			{
				break;
			}
		}
	}
	catch (exception e)
	{
		throw e;
	}
}

void readMassRange(ifstream& stream, vector<double> &range)
{
	vector<string> lineParts;
	string line;
	if (getline(stream, line))
	{
		if (!line.empty())
		{
			try
			{
				lineParts = split(line, ',');

				if (lineParts.size() == 2)
				{
					range.push_back(stof(lineParts.at(0)));
					range.push_back(stof(lineParts.at(1)));
				}
				else
				{
					throw string{ "Format of config is invalid: There were not exactly 2 entries for the mass range" };
				}
			}
			catch (exception e)
			{
				throw e;
			}
		}
		else
		{
			throw string{ "Format of config is invalid: There was no line for the mass range" };
		}
	}
	else
	{
		throw string{ "Format of config is invalid: There was no line for the mass range" };
	}
}

vector<double> readRange(ifstream& stream)
{
	vector<double> range;
	vector<string> lineParts;
	string line;

	try
	{
		// There should be 3 lines in the config file for the range, each containing two doubles
		for (int i = 0; i < 3; i++)
		{
			if (getline(stream, line))
			{
				if (!line.empty())
				{
					try
					{
						lineParts = split(line, ',');

						if (lineParts.size() == 2)
						{
							range.push_back(stof(lineParts.at(0)));
							range.push_back(stof(lineParts.at(1)));
						}
						else
						{
							throw string{ "Format of config is invalid: There were not exactly 2 entries per line for the ranges" };
						}
					}
					catch (exception e)
					{
						throw e;
					}
				}
				else
				{
					throw string{ "Format of config is invalid: There were less than 3 lines for the ranges" };
				}
			}
			else
			{
				throw string{ "Format of config is invalid: There were less than 3 lines for the ranges" };
			}
		}
	}
	catch (exception e)
	{
		throw e;
	}

	return range;
}

void readArray(ifstream& stream, vector<vec3d> &arr)
{
	vector<string> lineParts;
	string line;
	vec3d temp;

	try
	{
		// There should be N lines in the config file for the coordinates, each containing three doubles
		while (getline(stream, line))
		{

			// If line is empty, all coords have been read in -> break loop
			// Otherwise try to transform line into vec3d and add it to coords-vector
			if (!line.empty())
			{
				try
				{
					lineParts = split(line, ',');

					//xxTODO maybe accept 2 coords and set 3rd to zero for 2d-problem
					if (lineParts.size() == 3)
					{
						temp.x = stof(lineParts.at(0));
						temp.y = stof(lineParts.at(1));
						temp.z = stof(lineParts.at(2));

						arr.push_back(temp);
					}
					else
					{
						throw string{ "Format of config is invalid: There were not exactly 3 entries per line for the arrays" };
					}
				}
				catch (exception e)
				{
					throw e;
				}
			}
			else
			{
				break;
			}
		}
	}
	catch (exception e)
	{
		throw e;
	}
}

void initArrays_Root(long N, long N_loc, double*& masses, double*& coords, double*& coords_loc, double*& coords_step, double*& momenta, double*& momenta_loc, double*& force)
{
	masses = new double[N];

	coords = new double[3 * N];
	momenta = new double[3 * N];

	coords_loc = new double[3 * N_loc];
	momenta_loc = new double[3 * N_loc];

	// The second set of 3*N_loc entries in coords_step is used to store the received data
	coords_step = new double[3 * N_loc * 2];

	force = new double[3 * N_loc];
}

void initArrays_Worker(long N, long N_loc, double*& masses, double*& coords_loc, double*& coords_step, double*& momenta_loc, double*& force)
{
	masses = new double[N];

	coords_loc = new double[3 * N_loc];
	momenta_loc = new double[3 * N_loc];

	// The second set of 3*N_loc entries in coords_step is used to store the received data
	coords_step = new double[3 * N_loc * 2];

	force = new double[3 * N_loc];
}

void initArrays_Root_Hyper(long N, long N_loc, int layers, double* &masses, double* &coords, double* &coords_loc, double* &coords_step, double* &momenta, double* &momenta_loc, double* &force)
{
	masses = new double[N];

	coords = new double[3 * N];
	momenta = new double[3 * N];

	coords_loc = new double[3 * N_loc];
	momenta_loc = new double[3 * N_loc];

	coords_step = new double[3 * N_loc * layers];
	// Force array has one additional set of data used for storage when receiving the forces form the other processes
	force = new double[3 * N_loc * (layers+1)];
}

void initArrays_Worker_Hyper(long N, long N_loc, int layers, double* &masses, double* &coords_loc, double* &coords_step, double* &momenta_loc, double* &force)
{
	masses = new double[N];

	coords_loc = new double[3 * N_loc];
	momenta_loc = new double[3 * N_loc];

	coords_step = new double[3 * N_loc * layers];
	// Force array has one additional set of data used for storage when receiving the forces form the other processes
	force = new double[3 * N_loc * (layers+1)];
}

#pragma endregion

void readConfigClusters(string filename, long N, double* &masses, double* &coords, double* &momenta)
{
	string line;
	ifstream configFile(filename);

	if (configFile.is_open())
	{
		// Number of mass points
		long num;
		// Number of clusters
		int num_clusters;
		// Vector containing the masses of all mass points
		vector<double> m;
		// Range of coordinate values in each space-dimension (for setup with random numbers)
		vector<double> rangeCoord;
		vector<vector<double>> rangeCoord_Vec;
		// Range of veloctiy values in each space-dimension (for setup with random numbers)
		vector<double> rangeVel;
		vector<vector<double>> rangeVel_Vec;
		// Coordinate centers of clusters
		vector<vec3d> centers;

		// Bool used to determine whether the masses were given directly (false) 
		// or a range for random numbers was given (true)
		bool massIsRange = false;

		// Read configuration from file
		while (getline(configFile, line))
		{			
			if (line == "Number of Clusters")
			{
				try
				{
					getline(configFile, line);

					int temp = stoi(line);
					if (temp > 0)
					{
						num_clusters = temp;
					}
					else
					{
						throw string{ "Number of clusters was negative" };
					}
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading number of clusters: " + err };
					break;
				}
			}
			else if (line == "Masses")
			{
				try
				{
					readMasses(configFile, m);
					massIsRange = false;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading masses: " + err };
					break;
				}
			}
			else if (line == "Mass Range")
			{
				try
				{
					readMassRange(configFile, m);
					massIsRange = true;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading mass range: " + err };
					break;
				}
			}
			else if (line == "Cluster centers")
			{
				try
				{
					readArray(configFile, centers);
				}
				catch (string err)
				{
					throw string{ "Error when reading coordinates: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading coordinates: " + err };
					break;
				}
			}
			else if (line == "Coordinate Ranges")
			{
				try
				{
					for (int i = 0; i < num_clusters; i++)
					{
						rangeCoord = readRange(configFile);
						rangeCoord_Vec.push_back(rangeCoord);
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading range of coordinates: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading range of coordinates: " + err };
					break;
				}
			}
			else if (line == "Velocity Ranges")
			{
				try
				{
					for (int i = 0; i < num_clusters; i++)
					{
						rangeVel = readRange(configFile);
						rangeVel_Vec.push_back(rangeVel);
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading range of velocities: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading range of velocities: " + err };
					break;
				}
			}
		}

		configFile.close();

		// Check configuration for correctness
		bool isCorrect = true;
		string errString = "Configuration is invalid:";

		if (massIsRange)
		{
			if (m.size() != 2)
			{
				isCorrect = false;
				errString += "\n\t - Amount of given values for mass range is not correct (should be 2)";
			}
		}
		else
		{
			if (m.size() != N)
			{
				isCorrect = false;
				errString += "\n\t - Given number of mass points does not match number of given masses";
			}
		}
		for (int i = 0; i < num_clusters; i++)
		{
			if (rangeCoord_Vec[i].size() != 6)
			{
				isCorrect = false;
				errString += "\n\t - Amount of given values for coordinate range is not correct (should be 3 * 2)";
			}
			if (rangeVel_Vec[i].size() != 6)
			{
				isCorrect = false;
				errString += "\n\t - Amount of given values for velocity range is not correct (should be 3 * 2)";
			}
		}

		// If config is invalid, raise error
		if (!isCorrect)
		{
			throw errString;
		}
		// Else write into global arrays
		else
		{
			// Length of interval for random numbers
			double len_mass;
			double max = static_cast <double> (RAND_MAX);

			int N_cluster = N / num_clusters;

			// Init rand
			srand(time(NULL));

			// Fill arrays
			if (massIsRange)
			{
				len_mass = m[1] - m[0];
				for (int i = 0; i < N; i++)
				{
					masses[i] = static_cast <double> (rand()) / max * len_mass + m[0];
				}				
			}
			else
			{
				for (int i = 0; i < N; i++)
				{
					masses[i] = m[i];
				}				
			}
			
			for (int k = 0; k < num_clusters; k++)
			{
				for (int i = 0; i < N_cluster; i++)
				{
					coords[(k*N_cluster + i) * 3 + 0] = static_cast <double> (rand()) / max * (rangeCoord_Vec[k][1] - rangeCoord_Vec[k][0]) + rangeCoord_Vec[k][0] + centers[k].x;
					coords[(k*N_cluster + i) * 3 + 1] = static_cast <double> (rand()) / max * (rangeCoord_Vec[k][3] - rangeCoord_Vec[k][2]) + rangeCoord_Vec[k][2] + centers[k].y;
					coords[(k*N_cluster + i) * 3 + 2] = static_cast <double> (rand()) / max * (rangeCoord_Vec[k][5] - rangeCoord_Vec[k][4]) + rangeCoord_Vec[k][4] + centers[k].z;
					for (int j = 0; j < 3; j++)
					{
						momenta[(k*N_cluster + i) * 3 + j] = static_cast <double> (rand()) / max * (rangeVel_Vec[k][2 * j + 1] - rangeVel_Vec[k][2 * j]) + rangeVel_Vec[k][j * 2];
					}
				}
			}
		}
	}
	else
	{
		throw string{ "Cannot open config-file given filename:\t" + filename };
	}
}

void readConfigRand(string filename, long N, double* &masses, double* &coords, double* &momenta)
{
	string line;
	ifstream configFile(filename);

	if (configFile.is_open())
	{
		// Vector containing the masses of all mass points
		vector<double> m;
		// Range of coordinate values in each space-dimension (for setup with random numbers)
		vector<double> rangeCoord;
		// Range of veloctiy values in each space-dimension (for setup with random numbers)
		vector<double> rangeVel;

		// Bool used to determine whether the masses were given directly (false) 
		// or a range for random numbers was given (true)
		bool massIsRange = false;

		// Read configuration from file
		while (getline(configFile, line))
		{
			if (line == "Masses")
			{
				try
				{
					readMasses(configFile, m);
					massIsRange = false;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading masses: " + err };
					break;
				}
			}
			else if (line == "Mass Range")
			{
				try
				{
					readMassRange(configFile, m);
					massIsRange = true;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading mass range: " + err };
					break;
				}
			}
			else if (line == "Coordinate Ranges")
			{
				try
				{
					rangeCoord = readRange(configFile);
				}
				catch (string err)
				{
					throw string{ "Error when reading range of coordinates: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading range of coordinates: " + err };
					break;
				}
			}
			else if (line == "Velocity Ranges")
			{
				try
				{
					rangeVel = readRange(configFile);
				}
				catch (string err)
				{
					throw string{ "Error when reading range of velocities: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading range of velocities: " + err };
					break;
				}
			}
		}

		configFile.close();

		// Check configuration for correctness
		bool isCorrect = true;
		string errString = "Configuration is invalid:";

		if (massIsRange)
		{
			if (m.size() != 2)
			{
				isCorrect = false;
				errString += "\n\t - Amount of given values for mass range is not correct (should be 2)";
			}
		}
		else
		{
			if (m.size() != N)
			{
				isCorrect = false;
				errString += "\n\t - Given number of mass points does not match number of given masses";
			}
		}
		if (rangeCoord.size() != 6)
		{
			isCorrect = false;
			errString += "\n\t - Amount of given values for coordinate range is not correct (should be 3 * 2)";
		}
		if (rangeVel.size() != 6)
		{
			isCorrect = false;
			errString += "\n\t - Amount of given values for velocity range is not correct (should be 3 * 2)";
		}

		// If config is invalid, raise error
		if (!isCorrect)
		{
			throw errString;
		}
		// Else write into global arrays
		else
		{
			// Length of interval for random numbers
			double len_mass;
			double len_coord;
			double len_vel;
			double max = static_cast <double> (RAND_MAX);

			// Init rand
			srand(time(NULL));

			// Fill arrays
			for (int i = 0; i < N; i++)
			{
				if (massIsRange)
				{
					len_mass = m[1] - m[0];
					masses[i] = static_cast <double> (rand()) / max * len_mass + m[0];
				}
				else
				{
					masses[i] = m[i];
				}

				for (int j = 0; j < 3; j++)
				{
					len_coord = rangeCoord[j * 2 + 1] - rangeCoord[j * 2];
					coords[i * 3 + j] = static_cast <double> (rand()) / max * len_coord + rangeCoord[j * 2];

					len_vel = rangeVel[j * 2 + 1] - rangeVel[j * 2];
					momenta[i * 3 + j] = masses[i] * static_cast <double> (rand()) / max * len_vel + rangeVel[j * 2];
				}
			}
		}
	}
	else
	{
		throw string{ "Cannot open config-file given filename:\t" + filename };
	}
}

void readConfigFull(string filename, long N, double* &masses, double* &coords, double* &momenta)
{
	string line;
	ifstream configFile(filename);

	if (configFile.is_open())
	{
		// Pointer to array containing the masses of all mass points
		vector<double> m;
		// Coordinate values
		vector<vec3d> coordArr;
		// Veloctiy values
		vector<vec3d> velArr;

		// Read configuration from file
		while (getline(configFile, line))
		{
			
			if (line == "Masses")
			{
				try
				{
					readMasses(configFile, m);
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading masses: " + err };
					break;
				}
			}
			else if (line == "Coordinates")
			{
				try
				{
					readArray(configFile, coordArr);
				}
				catch (string err)
				{
					throw string{ "Error when reading coordinates: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading coordinates: " + err };
					break;
				}
			}
			else if (line == "Velocities")
			{
				try
				{
					readArray(configFile, velArr);
				}
				catch (string err)
				{
					throw string{ "Error when reading velocities: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading velocities: " + err };
					break;
				}
			}
		}

		configFile.close();

		// Check configuration for correctness
		bool isCorrect = true;
		string errString = "Configuration is invalid:";

		if (m.size() != N)
		{
			isCorrect = false;
			errString += "\n\t - Given number of mass points N (";
			errString += to_string(N);
			errString += ") does not match number of given masses (";
			errString += to_string(m.size());
			errString += ")";
		}
		if (coordArr.size() != N)
		{
			isCorrect = false;
			errString += "\n\t - Amount of given values for coordinates is not correct (should be 3 * N, but is 3 * ";
			errString += to_string(coordArr.size());
			errString += ")";
		}
		if (velArr.size() != N)
		{
			isCorrect = false;
			errString += "\n\t - Amount of given values for velocities is not correct (should be 3 * N, but is 3 * ";
			errString += to_string(velArr.size());
			errString += ")";
		}

		// If config is invalid, raise error
		if (!isCorrect)
		{
			throw errString;
		}
		// Else write into global arrays
		else
		{
			// Fill arrays
			for (int i = 0; i < N; i++)
			{
				masses[i] = m[i];

				coords[i * 3 + 0] = (coordArr[i].x);
				coords[i * 3 + 1] = (coordArr[i].y);
				coords[i * 3 + 2] = (coordArr[i].z);

				momenta[i * 3 + 0] = masses[i] * (velArr[i].x);
				momenta[i * 3 + 1] = masses[i] * (velArr[i].y);
				momenta[i * 3 + 2] = masses[i] * (velArr[i].z);
			}
		}
	}
	else
	{
		throw string{ "Cannot open file given filename:\t" + filename };
	}
}

#pragma endregion

void init(string configFilename, bool hyper, int size, int &printStep, int &steps, double &delta, double &epsilon, long &N, long &N_loc, int &layers, double* &masses, double* &coords, double* &coords_loc, double* &coords_step, double* &momenta, double* &momenta_loc, double* &force)
{
	string line;
	ifstream configFile(configFilename);
	int configType = -1;

	if (configFile.is_open())
	{
		// Read from file
		while (getline(configFile, line))
		{
			if (line == "Configuration Type")
			{
				try
				{
					getline(configFile, line);

					configType = stoi(line);
				}
				catch (string err)
				{
					throw string{ "Error when reading config type: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading config type: " + err };
					break;
				}
			}
			else if (line == "PrintStep")
			{
				try
				{
					getline(configFile, line);

					int temp = stoi(line);
					if (temp > 0)
					{
						printStep = temp;
					}
					else
					{
						throw string{ "Given print step was not positive" };
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading print step: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading print step: " + err };
					break;
				}
			}
			else if (line == "Steps")
			{
				try
				{
					getline(configFile, line);

					int temp = stoi(line);
					if (temp > 0)
					{
						steps = temp;
					}
					else
					{
						throw string{ "Given number of steps was not positive" };
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading steps: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading steps: " + err };
					break;
				}
			}
			else if (line == "Delta")
			{
				try
				{
					getline(configFile, line);

					double temp = stod(line);
					if (temp >= 0)
					{
						delta = temp;
					}
					else
					{
						throw string{ "Given delta was not positive" };
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading delta: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading delta: " + err };
					break;
				}
			}
			else if (line == "Epsilon")
			{
				try
				{
					getline(configFile, line);

					double temp = stod(line);
					if (temp >= 0)
					{
						epsilon = temp;
					}
					else
					{
						throw string{ "Given epsilon was not positive" };
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading epsilon: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading epsilon: " + err };
					break;
				}
			}
			else if (line == "Number of Masspoints")
			{
				try
				{
					getline(configFile, line);

					long temp = stol(line);
					if (temp > 0)
					{
						N = temp;
						if (N % size != 0)
						{
							throw string{ "Number of mass points is not evenly divisable by the number of processes!" };
						}
						N_loc = N / size;
					}
					else
					{
						throw string{ "N was negative" };
						break;
					}
				}
				catch (string err)
				{
					throw string{ "Error when reading number of mass points: " + err };
					break;
				}
				catch (exception e)
				{
					string err = e.what();
					throw string{ "Error when reading number of mass points: " + err };
					break;
				}
			}			
		}
		configFile.close();
	}
	else
	{
		throw string{ "Could not open given config-file: " + configFilename };
	}

	// Init arrays
	if (hyper)
	{
		initArrays_Root_Hyper(N, N_loc, layers, masses, coords, coords_loc, coords_step, momenta, momenta_loc, force);
	}
	else
	{
		initArrays_Root(N, N_loc, masses, coords, coords_loc, coords_step, momenta, momenta_loc, force);
	}

	switch (configType)
	{
	case 0:
		readConfigFull(configFilename, N, masses, coords, momenta);
		break;
	case 1:
		readConfigRand(configFilename, N, masses, coords, momenta);
		break;
	case 2:
		readConfigClusters(configFilename, N, masses, coords, momenta);
		break;
	default:
		throw string{ "Given config-type (" + to_string(configType) + ") was invalid!" };
	}
}

void clear_Arrays_Worker(double* &masses, double* &coords_loc, double* &coords_step, double* &momenta_loc, double* &force)
{
	delete masses;
	delete coords_loc;
	delete coords_step;
	delete momenta_loc;
	delete force;
}

#pragma endregion

#pragma region physics

// Gravitive force, eps is used to smoothen the potential and increase system stability
void force_grav(double* a, double* b, double m_a, double m_b, double eps, double* res)
{
	double fac = m_a * m_b * G / pow((pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2) + pow(a[2] - b[2], 2) + pow(eps, 2)), 1.5);

	res[0] = fac * (b[0] - a[0]);
	res[1] = fac * (b[1] - a[1]);
	res[2] = fac * (b[2] - a[2]);
}

// Computes all forces for one combination of two different masspoint-packages
void force_Combination(int rank_1, int rank_2, int force_index, double* coords_step_1, double* coords_step_2, double t, double eps, long N_loc, double* masses, double* force, double* force_temp)
{
	for (int i = 0; i < N_loc; i++)
	{
		// Compute force on i-th masspoint of package 1 using all mass points from package 2
		for (int j = 0; j < N_loc; j++)
		{
			force_grav(&coords_step_1[i * 3], &coords_step_2[j * 3], masses[rank_1*N_loc + i], masses[rank_2*N_loc + j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				// Combination 1-2 is stored in the first 3*N_loc components of the force-array 
				force[i * 3 + k] = force[i * 3 + k] + force_temp[k];
				// Combination 2-1 is stored in the 3*N_loc components of the force-array starting from force_index*3*N_loc
				// These forces are just the negative counterparts to Combination 1-2
				force[force_index * N_loc * 3 + j * 3 + k] = force[force_index * N_loc * 3 + j * 3 + k] - force_temp[k];
			}
		}
	}
}

// Computes all forces for one combination of two different masspoint-packages, storage at 2 different indices
void force_Combination_DualIndex(int rank, int rank_1, int rank_2, int force_index_1, int force_index_2, double* coords_step_1, double* coords_step_2, double t, double eps, long N_loc, double* masses, double* force, double* force_temp)
{
	for (int i = 0; i < N_loc; i++)
	{
		// Compute force on i-th masspoint of package 1 using all mass points from package 2
		for (int j = 0; j < N_loc; j++)
		{
			force_grav(&coords_step_1[i * 3], &coords_step_2[j * 3], masses[rank_1 * N_loc + i], masses[rank_2 * N_loc + j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				// Combination 1-2 is stored according to force_index_1
				force[force_index_1 * N_loc * 3 + i * 3 + k] = force[force_index_1 * N_loc * 3 + i * 3 + k] + force_temp[k];
				// Combination 2-1 is stored according to force_index_2
				force[force_index_2 * N_loc * 3 + j * 3 + k] = force[force_index_2 * N_loc * 3 + j * 3 + k] - force_temp[k];
			}
		}
	}
}

// Computes all forces for one combination of two different masspoint-packages
// But does not store the negative force in another component of the force array
void force_Combination_Single(int rank_1, int rank_2, double* coords_step_1, double* coords_step_2, double t, double eps, long N_loc, double* masses, double* force, double* force_temp)
{
	for (int i = 0; i < N_loc; i++)
	{
		// Compute force on i-th masspoint of package 1 using all mass points from package 2
		for (int j = 0; j < N_loc; j++)
		{
			force_grav(&coords_step_1[i * 3], &coords_step_2[j * 3], masses[rank_1*N_loc + i], masses[rank_2*N_loc + j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				// Combination 1-2 is stored in the first 3*N_loc components of the force-array 
				force[i * 3 + k] = force[i * 3 + k] + force_temp[k];
			}
		}
	}
}

#pragma endregion

#pragma region integrator

// Serial Leapfrog
void leap(double t, double eps, long N, double* masses, double* coords, double* coords_step, double* momenta, double* force, double* force_temp)
{
	// Calculate coords at half step
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			coords_step[i * 3 + j] = coords[i * 3 + j] + (t / (2.0 * masses[i])) * momenta[i * 3 + j];
		}
	}

	// Force computation
	// Exploit symmetry of gravitational force, i.e. f(i,j) = -f(j,i)
	for (int i = 0; i < N; i++)
	{
		// Set forces to 0
		for (int k = 0; k < 3; k++)
		{
			force[i * 3 + k] = 0.0;
		}

		// Compute force on i-th masspoint
		for (int j = 0; j < i; j++)
		{
			force_grav(&coords_step[i * 3], &coords_step[j * 3], masses[i], masses[j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				force[i * 3 + k] = force[i * 3 + k] + force_temp[k];
				force[j * 3 + k] = force[j * 3 + k] - force_temp[k];
			}
		}
	}

	// Calculate momenta at full step
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			momenta[i * 3 + j] = momenta[i * 3 + j] + t * force[i * 3 + j];
			coords[i * 3 + j] = coords_step[i * 3 + j] + (t / (2.0 * masses[i])) * momenta[i * 3 + j];
		}
	}
}

// Parallel Leapfrog - Systolic
void leap_MPI(int rank, int size, MPI_Status &status, int* neighbour_right, int* neighbour_left, double t, double eps, long N_loc, double* masses, double* coords_loc, double* coords_step, double* momenta_loc, double* force, double* force_temp)
{
	// Calculate coords at half step for all local coordinates
	for (int i = 0; i < N_loc; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			coords_step[i * 3 + j] = coords_loc[i * 3 + j] + (t / (2.0 * masses[rank * N_loc + i])) * momenta_loc[i * 3 + j];
			// Copy coordinates at half step to transient array
			coords_step[3 * N_loc + i * 3 + j] = coords_step[i * 3 + j];
		}
	}

	// First force computation for all combinations of local mass points
	for (int i = 0; i < N_loc; i++)
	{
		// Set all forces to 0
		for (int k = 0; k < 3; k++)
		{
			force[i * 3 + k] = 0.0;
		}

		// Compute force on i-th masspoint using all other local mass points
		for (int j = 0; j < i; j++)
		{
			force_grav(&coords_step[i * 3], &coords_step[j * 3], masses[rank * N_loc + i], masses[rank * N_loc + j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				force[i * 3 + k] = force[i * 3 + k] + force_temp[k];
				force[j * 3 + k] = force[j * 3 + k] - force_temp[k];
			}
		}
	}

	// Shifting the coords_step data and computing the forces
	for (int k = 1; k < size; k++)
	{
		MPI_Sendrecv_replace(&coords_step[N_loc * 3], N_loc * 3, MPI_DOUBLE,
			neighbour_right[0], 0, neighbour_left[0], 0,
			MPI_COMM_WORLD, &status);

		force_Combination_Single(rank, neighbour_left[k - 1], coords_step, &coords_step[N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	}

	// Calculate momenta at full step for all local mass points
	for (int i = 0; i < N_loc; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			momenta_loc[i * 3 + j] = momenta_loc[i * 3 + j] + t * force[i * 3 + j];
			coords_loc[i * 3 + j] = coords_step[i * 3 + j] + (t / (2.0 * masses[rank * N_loc + i])) * momenta_loc[i * 3 + j];
		}
	}
}

// Parallel Leapfrog - Hypersystolic (8 Processes)
void leap_MPI_Hyper_8(int rank, MPI_Status &status, int layers, int* shift_vector, int* neighbour_right, int* neighbour_left, double t, double eps, long N_loc, double* masses, double* coords_loc, double* coords_step, double* momenta_loc, double* force, double* force_temp)
{
	// Calculate coords at half step for all local coordinates
	for (int i = 0; i < N_loc; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			coords_step[i * 3 + j] = coords_loc[i * 3 + j] + (t / (2.0 * masses[rank*N_loc + i])) * momenta_loc[i * 3 + j];
		}
	}

	// First force computation for all combinations of local mass points
	for (int i = 0; i < N_loc; i++)
	{
		// Set all forces to 0
		for (int l = 0; l < layers; l++)
		{
			for (int k = 0; k < 3; k++)
			{
				force[l * N_loc * 3 + i * 3 + k] = 0.0;
			}
		}		

		// Compute force on i-th masspoint using all local mass points
		for (int j = 0; j < i; j++)
		{
			force_grav(&coords_step[i * 3], &coords_step[j * 3], masses[rank * N_loc + i], masses[rank * N_loc + j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				force[i * 3 + k] = force[i * 3 + k] + force_temp[k];
				force[j * 3 + k] = force[j * 3 + k] - force_temp[k];
			}
		}
	}

	// Shifting the coords_step data and computing the forces
	for (int k = 1; k < layers-1; k++)
	{
		MPI_Send(coords_step, N_loc * 3, MPI_DOUBLE, neighbour_right[k-1], 0, MPI_COMM_WORLD);
		MPI_Recv(&coords_step[k * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[k-1], 0, MPI_COMM_WORLD, &status);

		force_Combination(rank, neighbour_left[k-1], k, coords_step, &coords_step[k * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	}
	// Last step of force computation differs from the other steps, as only the combination (e.g. 1-2) is needed, 
	// without its counterpart (2-1)
	MPI_Send(coords_step, N_loc * 3, MPI_DOUBLE, neighbour_right[layers-2], 0, MPI_COMM_WORLD);
	MPI_Recv(&coords_step[(layers-1) * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[layers-2], 0, MPI_COMM_WORLD, &status);
	
	force_Combination_Single(rank, neighbour_left[layers-2], coords_step, &coords_step[(layers - 1) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	force_Combination_DualIndex(rank, neighbour_left[0], neighbour_left[layers-2], 1, layers-1, &coords_step[N_loc * 3], &coords_step[(layers - 1) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);

	// Shift forces backwards
	for (int k = layers-1; k > 0; k--)
	{		
		MPI_Send(&force[k * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[k-1], 0, MPI_COMM_WORLD);
		MPI_Recv(&force[layers * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_right[k-1], 0, MPI_COMM_WORLD, &status);
		
		// Add received force to local forces
		for (int i = 0; i < N_loc; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				force[i * 3 + j] = force[i * 3 + j] + force[layers * N_loc * 3 + i * 3 + j];
			}			
		}
	}

	// Calculate momenta at full step for all local mass points
	for (int i = 0; i < N_loc; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			momenta_loc[i * 3 + j] = momenta_loc[i * 3 + j] + t * force[i * 3 + j];
			coords_loc[i * 3 + j] = coords_step[i * 3 + j] + (t / (2.0 * masses[rank*N_loc + i])) * momenta_loc[i * 3 + j];
		}
	}
}

// Parallel Leapfrog - Hypersystolic (16 Processes)
void leap_MPI_Hyper_16(int rank, MPI_Status& status, int layers, int* shift_vector, int* neighbour_right, int* neighbour_left, double t, double eps, long N_loc, double* masses, double* coords_loc, double* coords_step, double* momenta_loc, double* force, double* force_temp)
{
	// Calculate coords at half step for all local coordinates
	for (int i = 0; i < N_loc; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			coords_step[i * 3 + j] = coords_loc[i * 3 + j] + (t / (2.0 * masses[rank * N_loc + i])) * momenta_loc[i * 3 + j];
		}
	}

	// First force computation for all combinations of local mass points
	for (int i = 0; i < N_loc; i++)
	{
		// Set all forces to 0
		for (int l = 0; l < layers; l++)
		{
			for (int k = 0; k < 3; k++)
			{
				force[l * N_loc * 3 + i * 3 + k] = 0.0;
			}
		}

		// Compute force on i-th masspoint using all local mass points
		for (int j = 0; j < i; j++)
		{
			force_grav(&coords_step[i * 3], &coords_step[j * 3], masses[rank * N_loc + i], masses[rank * N_loc + j], eps, force_temp);
			for (int k = 0; k < 3; k++)
			{
				force[i * 3 + k] = force[i * 3 + k] + force_temp[k];
				force[j * 3 + k] = force[j * 3 + k] - force_temp[k];
			}
		}
	}

	// Shifting the coords_step data and computing the forces
	for (int k = 1; k < layers - 2; k++)
	{
		MPI_Send(coords_step, N_loc * 3, MPI_DOUBLE, neighbour_right[k - 1], 0, MPI_COMM_WORLD);
		MPI_Recv(&coords_step[k * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[k - 1], 0, MPI_COMM_WORLD, &status);

		force_Combination(rank, neighbour_left[k - 1], k, coords_step, &coords_step[k * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	}

	// Last two steps of force computation differs from the previous steps, as more combinations are computed

	// Second to last step
	MPI_Send(coords_step, N_loc * 3, MPI_DOUBLE, neighbour_right[layers - 3], 0, MPI_COMM_WORLD);
	MPI_Recv(&coords_step[(layers - 2) * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[layers - 3], 0, MPI_COMM_WORLD, &status);

	force_Combination(rank, neighbour_left[layers - 3], layers - 2, coords_step, &coords_step[(layers - 2) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	force_Combination_DualIndex(rank, neighbour_left[0], neighbour_left[layers - 3], 1, layers - 2, &coords_step[N_loc * 3], &coords_step[(layers - 2) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	force_Combination_DualIndex(rank, neighbour_left[1], neighbour_left[layers - 3], 2, layers - 2, &coords_step[2 * N_loc * 3], &coords_step[(layers - 2) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);

	// Last step
	MPI_Send(coords_step, N_loc * 3, MPI_DOUBLE, neighbour_right[layers - 2], 0, MPI_COMM_WORLD);
	MPI_Recv(&coords_step[(layers - 1) * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[layers - 2], 0, MPI_COMM_WORLD, &status);

	force_Combination_Single(rank, neighbour_left[layers - 2], coords_step, &coords_step[(layers - 1) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	force_Combination_DualIndex(rank, neighbour_left[0], neighbour_left[layers - 2], 1, layers - 1, &coords_step[N_loc * 3], &coords_step[(layers - 1) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);
	force_Combination_DualIndex(rank, neighbour_left[1], neighbour_left[layers - 2], 2, layers - 1, &coords_step[2 * N_loc * 3], &coords_step[(layers - 1) * N_loc * 3], t, eps, N_loc, masses, force, force_temp);


	// Shift forces backwards
	for (int k = layers - 1; k > 0; k--)
	{
		MPI_Send(&force[k * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_left[k - 1], 0, MPI_COMM_WORLD);
		MPI_Recv(&force[layers * N_loc * 3], N_loc * 3, MPI_DOUBLE, neighbour_right[k - 1], 0, MPI_COMM_WORLD, &status);

		// Add received force to local forces
		for (int i = 0; i < N_loc; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				force[i * 3 + j] = force[i * 3 + j] + force[layers * N_loc * 3 + i * 3 + j];
			}
		}
	}

	// Calculate momenta at full step for all local mass points
	for (int i = 0; i < N_loc; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			momenta_loc[i * 3 + j] = momenta_loc[i * 3 + j] + t * force[i * 3 + j];
			coords_loc[i * 3 + j] = coords_step[i * 3 + j] + (t / (2.0 * masses[rank * N_loc + i])) * momenta_loc[i * 3 + j];
		}
	}
}

#pragma endregion

#pragma endregion